#!/usr/bin/env groovy 

def gv

properties([disableConcurrentBuilds()])

pipeline {
    agent any

    triggers { 
        pollSCM('* * * * *')
    }
    options {
        buildDiscarder(logRotator(numToKeepStr: '5', artifactNumToKeepStr: '5'))
        timestamps()
    }

    environment {
          MYVAR = 'is deploying'
          VERSION = "1.0.${BUILD_ID}_beta"
          deployThisStage = 'true'
          containerName = 'webapp-testcontainer'
          prodContainerName = 'webapp-prodcontainer'
          dockerImageName = 'anret/webapp-testenv' 
          dockerProdImage = 'anret/webapp-prod'
    }

    stages {
        stage('init') {
            steps {
                script {
                    gv = load "scripts/myscript.groovy"
                    echo " ========= Detecting changes in Git =========="
                    prevBuildGitCommitHash = currentBuild.previousSuccessfulBuild.buildVariables["GitCommitHash"]
                    sh "git diff --name-only ${prevBuildGitCommitHash} ${GIT_COMMIT}"
                    sh "chmod +x scripts/Dockercheck.sh"
                    branchName = sh(returnStdout: true, script: "git describe --all | awk -F'/' '/remotes/{print \$NF}'").trim()
                    echo " ========= Check if Docker Image has been updated =========="
                    echo "Checking branch name: ${branchName}."
                    buildDocker = sh (script: "git diff --name-only ${prevBuildGitCommitHash} ${GIT_COMMIT} | scripts/Dockercheck.sh", returnStdout: true).trim()
                    echo "Docker image has been updated since last build (yes/no): ${buildDocker}"

                }
                sh '''
                echo "========= Initialising / Loading scripts =========="
                echo "Checking content of the project."
                ls -la
                '''

            }
        }
        stage('building-test-env') {
            when {
                expression {
                    deployThisStage == 'true' && buildDocker == 'true'
                    //buildDocker == 'true'
                }
            }
            
            steps {

                echo "A Build number ${BUILD_ID} has been initiated."
                echo " ========= Logging in to Docker Hub =========="
                
                withCredentials([usernamePassword(credentialsId: 'dockerhub_anret', usernameVariable: 'USER', passwordVariable: 'PASS')]) {
                sh ''' 
                docker login -u $USER -p $PASS
                '''
                }
                
                echo " ========= start building image =========="
                sh "docker build -t ${dockerImageName} test/"
                sh "docker images"
                echo " ========= image push to Docker Hub =========="
                sh "docker push ${dockerImageName}"
            }
        }

        stage('preparing-test-env') {
            when {
                expression {
                    deployThisStage == 'true'
                }
            }
            steps {
                echo " ========= Pulling latest image from Docker Hub =========="
                sh "docker pull ${dockerImageName}"
                sh 'docker images'
                echo " ========= Start a container =========="
                sh "docker run --name ${containerName} --volume ${WORKSPACE}:/home/app -id ${dockerImageName}"
                sh "docker exec -i ${containerName} ls -la /home/app"
            }
        }
        
        stage('static-code-analysis') {
            when {
                expression {
                    deployThisStage == 'true'
                }
            }
            steps {
                script {
                    gv.testApp()
                      }
                echo " ========= Checking project structure =========="
                sh "docker exec -i ${containerName} ls -la /home/app"
                sh "tree ${WORKSPACE}"
                echo " ========= Running static code analysis =========="
                sh "docker exec -i ${containerName} flake8 /home/app/src/website.py --exit-zero --output-file /home/app/testreport/flake8-output.txt"
                sh "docker exec -i ${containerName} cat /home/app/testreport/flake8-output.txt"
                sh "docker exec -i ${containerName} flake8_junit /home/app/testreport/flake8-output.txt /home/app/testreport/flake8-output.xml"
                sh "docker exec -i ${containerName} cat /home/app/testreport/flake8-output.xml"
                sh "tree ${WORKSPACE}"
                junit "**/testreport/**.xml"

            }
        }

        stage('build') {
            when {
                expression {
                    deployThisStage == 'true'
                }
            }
            steps {
                script {
                    gv.buildApp()
                }
                echo " ========= building production image =========="
                sh "cp -r ${WORKSPACE}/src/* ${WORKSPACE}/prod"
                sh "docker build -t ${dockerProdImage} prod/" 
                sh "docker run --name ${prodContainerName} --network host -d ${dockerProdImage}"
                sh "sleep 5"
            }
        }

        stage('integration-tests') {
            when {
                expression {
                    deployThisStage == 'true'
                }
            }
            steps {
                script {
                    gv.testApp()
                    sh "chmod +x ${WORKSPACE}/scripts/request.py"
                    echo " ========= Running intergation tests =========="   
                    sh "scripts/request.py"
                    env.integtationTestPassed = sh (script: "scripts/request.py", returnStdout: true).trim()
                    echo "Integration tests passed: ${integtationTestPassed}"
                    }
            }
        }
        
        stage('deploy') {
            when {
                expression {
                    deployThisStage == 'true' && integtationTestPassed == 'true'
                }
            }
            steps {
                script {
                    gv.deployApp()
                      }
                echo " ========= Logging in to Docker Hub =========="
                withCredentials([usernamePassword(credentialsId: 'dockerhub_anret', usernameVariable: 'USER', passwordVariable: 'PASS')]) {
                sh ''' 
                docker login -u $USER -p $PASS
                '''
                }
                echo " ========= image push to Docker Hub =========="
                sh "docker push ${dockerProdImage}"




                echo "Version: ${VERSION} has been deployed, because deployment is set to ${deployThisStage} and integration tests have been passed."
            }
        }

    }    
    post {
        always {
            script{
                env.GitCommitHash = "${GIT_COMMIT}"
            }
            sh "docker stop ${containerName} && docker rm ${containerName}"
            sh "docker stop ${prodContainerName} && docker rm ${prodContainerName}"
        }
    }
}
